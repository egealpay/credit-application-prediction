# Credit Application Prediction

## Preprocessing

To be able to apply machine learning algorithms on the data we are given, we needed to apply some data preprocessing. 
However, before explaining the details of the preprocessing, the clear explanation of the data should be given.

There are 537870 rows in the data and there are 45 features, 13 of them are categorical and rest is numerical. 
Some features don’t have any NaN values, however some of them has many NaN values.

First of all, we have removed following columns with these reasons:

* id → It doesn’t have any effect 
* desc → %99 of samples contain NaN
* annual_inc_joint → %99 of samples contain NaN
* dti_joint → %99 of samples contain NaN
* inq_last_12m → %57 of samples contain NaN
* total_cu_tl → %57 of samples contain NaN
* inq_fi → %57 of samples contain NaN
* all_util → %57 of samples contain NaN
* max_bal_bc → %57 of samples contain NaN
* il_util → %63 of samples contain NaN
* total_bal_il → %57 of samples contain NaN
* emp_title → It has too many categories
* zip_code → It has too many categories 
* earliest_cr_line → It has too many categories 
* title → Since there are some overlapping categories with 'purpose' Feature we will use ‘purpose’ Feature
* addr_state → It has too many categories 

After removing these features, we started to deal with categorical features. 
In this dataset, there are two different types of categorical features:

1. Ordinal: To convert this type of categorical feature to numeric, we have mapped each category to an integer. There was only one feature which was ordinal:
   * emp_length

2. Nominal: To convert this type of categorical feature to numeric, we created dummy variables for each category. These were the features which were nominal:
   * term
   * home_ownership
   * verification_status
   * purpose
   * initial_list_status
   * application_type

After these steps, we have splitted our dataset into training data and validation data. 

There were still some features with NaN values. We applied following procedures to replace NaN values:
* emp_length → Fill with mode
* dti → Fill with median
* inq_last_6mths → Fill with median

The most important thing in here is to split the data before imputation. 
If we have done imputation first, then it would leak validation data information to training data.



## Models

After we were done with data preprocessing, we started to create a model. But there was a problem with data, it was unbalanced. 
Our target label “Charged_Off” consists of two classes, one as -1 for good applicants who pay their debts, and +1 for bad applicants who don’t. 
Ratio of these classes is close to 4:1. 
To overcome this, we have used three approaches as Oversampling, Undersampling and an approach where we just used the data without any kind of sampling.

Oversampling is the method of copying the data of less used class to match the more used class data. 
Whereas, undersampling is dropping some data of the class that has more data than the other to match the total data size.

In each approach we have decided to use majority voting. 
Each models decision is considered as vote. In our case it is either -1 or +1. 
For each case we let each model decide its own result and vote for result as a vote. 
The highest vote is considered as the end result. This is majority voting. 


### Oversampling

In oversampling, we first read the data by accessing the drive. 
After doing the procedures mentioned in preprocessing we applied random oversampling to balance the data.

We splitted our data into test and train with test size of 15% before filling the NaN’s as mentioned with the procedures in preprocessing.

We used a decision tree to train our model and got ≈0.55 Area Under the ROC curve (AUC) score. The model also had an accuracy of ≈0.67 in test set. 
A random forest to train our model and got ≈0.56 AUC score. The model also had an accuracy of ≈0.75 in test set.
An Ada Boost to train our model and got ≈0.6469 AUC score. The model also had an accuracy of ≈0.6493 in test set.

After seeing each model perform we decided to use majority voting. Our majority voting got ≈0.59 AUC score, accuracy of ≈0.72 in test set.



### Without Any Kind of Sampling

In this model without sampling, we first read the data by accessing the drive. 
After doing the procedures mentioned in preprocessing, without applying sampling to balance the data, 
we splitted our data into test and train with test size of 15% before filling the NaN’s as mentioned with the procedures in preprocessing.

We used a decision tree to train our model and got ≈0.556 Area Under the ROC curve (AUC) score. 
The model also had an accuracy of ≈0.674 in test set. 

A random forest to train our model and got ≈0.541 AUC score. The model also had an accuracy of ≈0.761 in test set.

An Ada Boost to train our model and got ≈0.55 AUC score. The model also had an accuracy of ≈0.773 in test set.

After seeing each model perform we decided to use majority voting. Our majority voting got ≈0.546 AUC score,  accuracy of ≈0.769 in test set.

In addition to that, we wanted to try XGBoost. An XGBoost to train our model and got ≈0.54 AUC score. The model also had an accuracy of ≈0.774 in test set.


### Undersampling

In undersampling, we first read the data by accessing the drive. 
After doing the procedures mentioned in preprocessing we applied random undersampling to balance the data.

We splitted our data into test and train with test size of 15% before filling the NaN’s as mentioned with the procedures in preprocessing.

We used a decision tree to train our model and got ≈0.5644 Area Under the ROC curve (AUC) score. The model also had an accuracy of ≈0.5653 in test set.
A random forest to train our model and got ≈0.60 AUC score. The model also had an accuracy of ≈0.65 in test set. 
An Ada Boost to train our model and got ≈0.64 AUC score. The model also had an accuracy of ≈0.65 in test set.

After seeing each model perform we decided to use majority voting. Our majority voting got ≈0.63 AUC score, accuracy of ≈0.65 in test set.
	

## Conclusion

Consequently, we have decided to share the notebook with Oversampling model, as our project’s main file. 
To make testing easy, we have done reading the test data, applying preprocessing and making prediction. 
In addition to that, in case of data inbalance, instead of accuracy we need to use other metrics to evaluate our model. 
ROC can be used for this aim.
